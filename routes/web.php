<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('book/{id}', [
    'as'   => 'book_view',
    'uses' => 'BookController@show'
]);

$router->post('book/', [
    'as'   => 'book_create',
    'uses' => 'BookController@create'
]);

$router->get('author/{id}', [
    'as'   => 'author_view',
    'uses' => 'AuthorController@show'
]);

$router->post('author/', [
    'as'   => 'author_create',
    'uses' => 'AuthorController@create'
]);
