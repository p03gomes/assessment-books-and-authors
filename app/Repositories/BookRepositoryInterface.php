<?php

namespace App\Repositories;

use App\Models\Book;

interface BookRepositoryInterface
{
    /**
     * @param $id
     * @return Book|null
     */
    public function findById($id): ?Book;

    /**
     * @param array $data
     * @return Book
     */
    public function create(array $data): Book;
}
