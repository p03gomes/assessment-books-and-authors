<?php


namespace App\Repositories;


use App\Models\Author;

class AuthorRepositoryEloquent implements AuthorRepositoryInterface
{
    /**
     * @var Author
     */
    private $model;

    /**
     * AuthorRepositoryEloquent constructor.
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        $this->model = $author;
    }

    /**
     * @param $id
     * @return Author|null
     */
    public function findById($id): ?Author
    {
        return $this->model->with('books')->find($id);
    }

    /**
     * @param array $data
     * @return Author
     */
    public function create(array $data): Author
    {
        return $this->model->create($data);
    }
}
