<?php

namespace App\Repositories;

use App\Models\Book;

class BookRepositoryEloquent implements BookRepositoryInterface
{
    /**
     * @var Book
     */
    private $model;

    /**
     * BookRepositoryEloquent constructor.
     * @param Book $books
     */
    public function __construct(Book $books)
    {
        $this->model = $books;
    }

    /**
     * @param $id
     * @return Book|null
     */
    public function findById($id): ?Book
    {
        return $this->model->with('authors')->find($id);
    }

    /**
     * @param array $data
     * @return Book
     */
    public function create(array $data): Book
    {
        $book = $this->model->create($data);

        $book->authors()->sync($data['authors']);

        return $this->findById($book->getKey());
    }

}
