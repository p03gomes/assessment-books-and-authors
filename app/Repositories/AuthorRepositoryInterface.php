<?php

namespace App\Repositories;

use App\Models\Author;

interface AuthorRepositoryInterface
{
    /**
     * @param $id
     * @return Author|null
     */
    public function findById($id): ?Author;

    /**
     * @param array $data
     * @return Author
     */
    public function create(array $data): Author;
}
