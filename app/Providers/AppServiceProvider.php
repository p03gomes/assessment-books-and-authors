<?php

namespace App\Providers;

use App\Repositories\AuthorRepositoryEloquent;
use App\Repositories\AuthorRepositoryInterface;
use App\Repositories\BookRepositoryEloquent;
use App\Repositories\BookRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookRepositoryInterface::class, BookRepositoryEloquent::class);
        $this->app->bind(AuthorRepositoryInterface::class, AuthorRepositoryEloquent::class);
    }
}
