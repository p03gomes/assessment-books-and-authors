<?php

namespace App\Services;

use App\Exceptions\AuthorNotFoundException;
use App\Exceptions\ValidationException;
use App\Models\Author;
use App\Repositories\AuthorRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class AuthorService
{
    /**
     * @var AuthorRepositoryInterface
     */
    private $authorRepository;

    /**
     * BookService constructor.
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @param $id
     * @return Author
     * @throws AuthorNotFoundException
     */
    public function findById($id): Author
    {
        $book = $this->authorRepository->findById($id);

        if (null === $book) {
            throw new AuthorNotFoundException();
        }

        return $book;
    }

    /**
     * @param array $data
     * @return Author
     * @throws ValidationException
     */
    public function create(array $data): Author
    {
        $validator = Validator::make($data, [
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->authorRepository->create($data);
    }
}
