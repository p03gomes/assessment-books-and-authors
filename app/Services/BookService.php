<?php

namespace App\Services;

use App\Exceptions\BookNotFoundException;
use App\Exceptions\ValidationException;
use App\Models\Book;
use App\Repositories\BookRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class BookService
{
    /**
     * @var BookRepositoryInterface
     */
    private $bookRepository;

    /**
     * BookService constructor.
     * @param BookRepositoryInterface $bookRepository
     */
    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * @param $id
     * @return Book
     */
    public function findById($id): Book
    {
        $book = $this->bookRepository->findById($id);

        if (null === $book) {
            throw new BookNotFoundException();
        }

        return $book;
    }

    /**
     * @param array $data
     * @return Book|null
     * @throws ValidationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(array $data): ?Book
    {
        $validator = Validator::make($data, [
            'isbn' => ['required','unique:books','regex:/(\b[0-9]{10}$\b)|(\b[0-9]{13}$\b)/'],
            'title' => 'required',
            'authors' => 'required|array',
            'authors.*' => 'integer|exists:authors,id'
        ], [
            'authors.required' => 'You must specify the author/authors.',
            'authors.array' => 'Authors should be a list of ids.',
            'authors.*.integer' => 'Author id invalid.',
            'authors.*.exists' => 'Author not found.',
            'isbn.regex' => 'ISBN must have either 10 or 13 digits (numbers only)'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->bookRepository->create($data);
    }
}
