<?php

namespace App\Http\Controllers;

use App\Exceptions\BookNotFoundException;
use App\Exceptions\ValidationException;
use App\Services\BookService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookController extends Controller
{
    /**
     * @var BookService
     */
    private $bookService;

    /**
     * BookController constructor.
     * @param BookService $bookService
     */
    public function __construct(BookService $bookService) {
        $this->bookService = $bookService;
    }

    /**
     * @OA\Get(path="/book/{id}",
     *   tags={"Book"},
     *   summary="Returns a book with its authors",
     *   operationId="getBook",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     @OA\Schema(type="int")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Book data",
     *   ),
     *   @OA\Response(
     *     response="404",
     *     description="Error: Book not found",
     *   ),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $book = $this->bookService->findById($id);
            return response()->json($book);
        } catch (BookNotFoundException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @OA\Post(path="/book",
     *   tags={"Book"},
     *   summary="Creates a book with its authors",
     *   operationId="create book",
     *   @OA\RequestBody(
     *     description="Book to add",
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(
     *          @OA\Property(property="title", type="string", description="Book title", maximum=255),
     *          @OA\Property(property="isbn", type="string", description="ISBN10 or ISBN13"),
     *          @OA\Property(property="authors", type="array", description="Author id list", @OA\Items(type="integer"))
     *       ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successful operation",
     *   ),
     *   @OA\Response(
     *     response="422",
     *     description="Error: Validation failed",
     *   ),
     * )
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $book = $this->bookService->create($request->toArray());
            return response()->json($book, Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage(), $e->getCode(), $e->errors());
        }
    }
}
