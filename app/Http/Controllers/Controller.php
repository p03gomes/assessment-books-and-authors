<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="Book/Author API",
     *   version="1.0",
     *   @OA\Contact(
     *     email="p03gomes@gmail.com",
     *     name="Pedro Gomes"
     *   )
     * )
     */

    /**
     * @param string $message
     * @param array $errors
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function error(string $message, int $status = Response::HTTP_INTERNAL_SERVER_ERROR, array $errors = [])
    {
        return response()->json(
            ['message' => $message, 'errors' => $errors],
            $status
        );
    }
}
