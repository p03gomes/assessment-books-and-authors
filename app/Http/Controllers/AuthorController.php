<?php

namespace App\Http\Controllers;

use App\Exceptions\AuthorNotFoundException;
use App\Exceptions\ValidationException;
use App\Services\AuthorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    /**
     * @var AuthorService
     */
    private $authorService;

    /**
     * AuthorController constructor.
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * @OA\Get(path="/author/{id}",
     *   tags={"Author"},
     *   summary="Returns an author with his/her books",
     *   operationId="getAuthor",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     @OA\Schema(type="int")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Author data",
     *   ),
     *   @OA\Response(
     *     response="404",
     *     description="Error: Author not found",
     *   ),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $author = $this->authorService->findById($id);
            return response()->json($author);
        } catch (AuthorNotFoundException $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @OA\Post(path="/author",
     *   tags={"Author"},
     *   summary="Creates an author",
     *   operationId="createAuthor",
     *   @OA\RequestBody(
     *     description="Author to add",
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/author")
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Author created",
     *   ),
     *   @OA\Response(
     *     response="422",
     *     description="Error: Validation failed",
     *   ),
     * )
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $author = $this->authorService->create($request->toArray());
            return response()->json($author, Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            return $this->error($e->getMessage(), $e->getCode(), $e->errors());
        }
    }
}
