<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class AuthorNotFoundException extends ModelNotFoundException
{
    protected $code = Response::HTTP_NOT_FOUND;

    protected $message = "Author not found";
}
