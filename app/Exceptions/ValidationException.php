<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException as Validation;

class ValidationException extends Validation
{
    protected $code = Response::HTTP_UNPROCESSABLE_ENTITY;
}
