<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

/**
 * @OA\Schema(
 *     schema="book",
 *     title="Book",
 *     description="Book model",
 * )
 *
 * @OA\Property(property="id", type="integer", description="Identifier", readOnly=true)
 * @var int        $id
 *
 * @OA\Property(property="isbn", type="string", description="ISBN10 or ISBN13")
 * @var string     $isbn
 *
 * @OA\Property(property="title", type="string", description="Book title", maximum=255)
 * @var string     $title
 *
 */
class Book extends Model
{
    use HasFactory;
    /**
     * @var array
     */
    protected $fillable = ['isbn', 'title'];

    /**
     * @var string[]
     */
    protected $hidden = ['pivot', 'created_at', 'updated_at'];

    /**
     * @return Relations\BelongsToMany
     */
    public function authors(): Relations\BelongsToMany
    {
        return $this->belongsToMany(Author::class, 'book_author');
    }
}
