<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;


/**
 * @OA\Schema(
 *     schema="author",
 *     title="Author",
 *     description="Author model",
 * )
 *
 * @OA\Property(property="id", type="integer", description="Identifier", readOnly=true)
 * @var int        $id
 *
 * @OA\Property(property="first_name", type="string", description="First Name", maximum=255)
 * @var string     $isbn
 *
 * @OA\Property(property="last_name", type="string", description="Last Name", maximum=255)
 * @var string     $title
 */
class Author extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name'];

    /**
     * @var string[]
     */
    protected $hidden = ['pivot', 'created_at', 'updated_at'];

    /**
     * @return Relations\BelongsToMany
     */
    public function books(): Relations\BelongsToMany
    {
        return $this->belongsToMany(Book::class, 'book_author');
    }
}
