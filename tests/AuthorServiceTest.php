<?php

use App\Exceptions\AuthorNotFoundException;
use App\Exceptions\ValidationException;
use App\Models\Author;
use App\Repositories\AuthorRepositoryInterface;
use App\Services\AuthorService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthorServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function testFindById()
    {
        $authorRepository = Mockery::mock(AuthorRepositoryInterface::class);
        $authorRepository->shouldReceive('findById')->once()->andReturn(new Author());
        $authorService = new AuthorService($authorRepository);
        $author = $authorService->findById(0);
        $this->assertInstanceOf(Author::class, $author);
    }

    public function testExceptionFindById()
    {
        $authorRepository = Mockery::mock(AuthorRepositoryInterface::class);
        $authorRepository->shouldReceive('findById')->once()->andReturn(null);
        $authorService = new AuthorService($authorRepository);
        $this->expectException(AuthorNotFoundException::class);
        $authorService->findById(0);
    }

    public function testCreate()
    {
        $authorRepository = Mockery::mock(AuthorRepositoryInterface::class);
        $authorRepository->shouldReceive('create')->once()->andReturn(new Author());

        $authorService = new AuthorService($authorRepository);
        $authorData = Author::factory()->make()->toArray();

        $author = $authorService->create($authorData);
        $this->assertInstanceOf(Author::class, $author);
    }

    public function testValidationExceptionCreate()
    {
        $authorRepository = Mockery::mock(AuthorRepositoryInterface::class);
        $authorRepository->shouldReceive('create')->never()->andReturn(new Author());
        $authorService = new AuthorService($authorRepository);
        $this->expectException(ValidationException::class);
        $authorService->create([]);
    }
}
