<?php

use App\Models\Author;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;


class AuthorControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetAuthor() {
        $author = Author::factory()->create();

        $this->get('/author/'.$author->id);
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->seeJsonStructure(
            [
                'id',
                'first_name',
                'last_name',
                'books' => [
                    '*' => ['id','isbn','title']
                ],
            ]
        );
    }

    public function testGetAuthorNotFound() {
        $this->get('/author/0');
        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }

    public function testCanCreateAuthor() {
        $author = Author::factory()->make()->toArray();

        $this->post('/author',$author);
        $this->assertResponseStatus(Response::HTTP_CREATED);
        $this->seeJsonStructure(
            [
                'id',
                'first_name',
                'last_name'
            ]
        );
    }

    public function testCreateAuthorValidationFail() {
        $author = [];

        $this->post('/author',$author);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJsonStructure(['errors', 'message']);
    }
}
