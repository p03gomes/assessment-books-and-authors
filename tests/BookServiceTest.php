<?php

use App\Exceptions\BookNotFoundException;
use App\Exceptions\ValidationException;
use App\Models\Author;
use App\Models\Book;
use App\Repositories\BookRepositoryInterface;
use App\Services\BookService;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BookServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function testFindById()
    {
        $bookRepository = Mockery::mock(BookRepositoryInterface::class);
        $bookRepository->shouldReceive('findById')->once()->andReturn(new Book());
        $bookService = new BookService($bookRepository);
        $book = $bookService->findById(0);
        $this->assertInstanceOf(Book::class, $book);
    }

    public function testExceptionFindById()
    {
        $bookRepository = Mockery::mock(BookRepositoryInterface::class);
        $bookRepository->shouldReceive('findById')->once()->andReturn(null);
        $bookService = new BookService($bookRepository);
        $this->expectException(BookNotFoundException::class);
        $bookService->findById(0);
    }

    public function testCreate()
    {
        $bookRepository = Mockery::mock(BookRepositoryInterface::class);
        $bookRepository->shouldReceive('create')->once()->andReturn(new Book());

        $bookService = new BookService($bookRepository);

        $bookData = Book::factory()->make()->toArray();
        $author = Author::factory()->create();
        $bookData['authors'] = [ $author->id ];

        $book = $bookService->create($bookData);
        $this->assertInstanceOf(Book::class, $book);
    }

    public function testValidationExceptionCreate()
    {
        $bookRepository = Mockery::mock(BookRepositoryInterface::class);
        $bookRepository->shouldReceive('create')->never()->andReturn(new Book());
        $bookService = new BookService($bookRepository);
        $this->expectException(ValidationException::class);
        $bookService->create([]);
    }
}
