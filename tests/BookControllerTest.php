<?php

use App\Models\Author;
use App\Models\Book;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;

class BookControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetBook() {
        $book = Book::factory()->create();

        $this->get('/book/'.$book->id);
        $this->assertResponseStatus(Response::HTTP_OK);
        $this->seeJsonStructure(
            [
                'id',
                'isbn',
                'title',
                'authors' => [
                    '*' => ['id','first_name','last_name']
                ],
            ]
        );
    }

    public function testGetBookNotFound() {
        $this->get('/book/0');
        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }

    public function testCanCreateBook() {

        $author = Author::factory()->create();
        $book = Book::factory()->make()->toArray();

        $book['authors'] = [
            $author->id
        ];

        $this->post('/book',$book);
        $this->assertResponseStatus(Response::HTTP_CREATED);
        $this->seeJsonStructure(
            [
                'id',
                'isbn',
                'title',
                'authors' => [
                    '*' => ['id','first_name','last_name']
                ],
            ]
        );
    }

    public function testCreateBookValidationFail() {
        $book = [];
        $this->post('/book',$book);
        $this->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJsonStructure(['errors', 'message']);
    }
}
