# Author/Book API
## Requirements
- PHP >= 7.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Mysql

## Installation 
Clone the repository

Run `composer install` to install the dependencies 

Create local database

Copy `.env.example` to `.env`, change db settings to local db values

For more information about environment configuration access: 

https://lumen.laravel.com/docs/8.x/configuration#environment-configuration

Run `php artisan migrate` to run database migrations

Run `php -S localhost:8000 -t public` to start PHP development server

## Tests
Run tests locally:
```bash
vendor/bin/phpunit
```

## API Documentation
To access the current documentation:

http://localhost:8000/documentation

To generate a new documentation from annotations: 
```bash
php artisan swagger-lume:generate
```

## Lumen PHP Framework Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## License


The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
